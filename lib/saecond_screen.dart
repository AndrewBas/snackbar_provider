import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'counter_provider.dart';

class SecondScreen extends StatelessWidget {
  CounterProvider cp = CounterProvider.instance;

  void _incrementCounter() {
    print('_incrementCounter ');
    cp.addCounter();
    /*setState(() {
      _counter++;
    });*/
  }

  void _decrementCounter() {
    print('_decrementCounter ');

    cp.removeCounter();
  }

  @override
  Widget build(BuildContext context) {
    var itemData = Provider.of<CounterProvider>(context);
    final counterItem = itemData.counter;
    return Scaffold(
      appBar: AppBar(
        title: Text('title'),
      ),
      body: Builder(
        builder: (context) =>
        Center(
          // Center is a layout widget. It takes a single child and positions it
          // in the middle of the parent.
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              IconButton(
                  icon: Icon(Icons.add),
                  onPressed: () {
                    _incrementCounter();
                    Scaffold.of(context).showSnackBar(SnackBar(
                      content: Text('$counterItem'),
                      duration: Duration(microseconds: 300),
                    ));
                  }),
              IconButton(icon: Icon(Icons.remove), onPressed: (){
                _decrementCounter();
                Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text('$counterItem'),
                  duration: Duration(microseconds: 300),
                ));
              }),
            ],
          ),
        ),
      ),

    );
  }
}
